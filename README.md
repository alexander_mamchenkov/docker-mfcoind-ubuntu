# docker-mfcoind-ubuntu

MFCoind docker image based on Ubuntu 16.04

You can pass RPCUSER and RPCPASS env variables to change defaults:

User: MFCoinrpc
Pass: MFCoinrpcPass

Exposed ports:

* 22823: JSON-RPC 
* 22824: P2P Data

This image will also pull for latest available chain status from:
https://bitbucket.org/alexander_mamchenkov/mfcoind-data.git

DockerHub: https://hub.docker.com/r/mamchenkov/mfcoind-centos/
