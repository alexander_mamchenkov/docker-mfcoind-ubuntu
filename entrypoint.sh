#!/bin/bash

cd /root
wget https://www.dropbox.com/s/f6a3465zudvltz9/latest.tar.gz?dl=1 -O latest.tar.gz
tar -xzf latest.tar.gz
rm latest.tar.gz
echo "rpcuser=$RPCUSER" >> /root/.MFCoin/MFCoin.conf
echo "rpcpassword=$RPCPASS" >> /root/.MFCoin/MFCoin.conf
MFCoind -daemon -txindex && tail -f /root/.MFCoin/debug.log
